package pl.piotrkulma.graphic3d;

/**
 * Created by Piotr Kulma on 26.06.2017.
 */
public class Pointd {
    private double x;
    private double y;
    private double z;

    public Pointd(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public void copy(Pointd p3d) {
        this.x = p3d.getX();
        this.y = p3d.getY();
        this.z = p3d.getZ();
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }
}

