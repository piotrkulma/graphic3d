package pl.piotrkulma.graphic3d;

/**
 * Created by Piotr Kulma on 26.06.2017.
 */
public final class FloatMathUtils {
    private FloatMathUtils() {
    }

    public double getFunctionBetween(double xa, double ya, double xb, double yb, double x) {
        return ((ya - yb) / (xa - xb)) * x + (ya - (((ya - yb) / (xa - xb)) * xa));
    }

    public static double[] projection(Pointd pointf, double d, double scale, double screenMidX, double screenMidY) {
        return new double[]
                {
                        ((pointf.getX() * d / (pointf.getZ() + d)) * scale) + screenMidX,
                        ((pointf.getY() * d / (pointf.getZ() + d)) * scale) + screenMidY
                };
    }

    public static Pointd rotateX(Pointd p, double angle) {
        double rad = angleToRad(angle);
        return new Pointd(
                p.getX(),
                p.getY() * Math.cos(rad) - p.getZ() * Math.sin(rad),
                p.getY() * Math.sin(rad) + p.getZ() * Math.cos(rad));
    }

    public static double angleToRad(double angle) {
        return angle * (Math.PI / 180d);
    }
}
