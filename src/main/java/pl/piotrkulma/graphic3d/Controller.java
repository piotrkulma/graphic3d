package pl.piotrkulma.graphic3d;

import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import org.apache.log4j.Logger;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

public class Controller {
    private final static Logger LOG = Logger.getLogger(Controller.class);

    private Renderer renderer;

    @FXML
    protected Canvas canvas3d;

    @FXML
    protected void initialize() throws Exception{
        List<Pointd> drawingList1 = new ArrayList<Pointd>();
        drawingList1.add(new Pointd(-1, 1, 1));
        drawingList1.add(new Pointd(1, 1, 1));
        drawingList1.add(new Pointd(1, -1, 1));
        drawingList1.add(new Pointd(-1, -1, 1));

        List<Pointd> drawingList2 = new ArrayList<Pointd>();
        drawingList2.add(new Pointd(-1, 1, -1));
        drawingList2.add(new Pointd(1, 1, -1));
        drawingList2.add(new Pointd(1, -1, -1));
        drawingList2.add(new Pointd(-1, -1, -1));

        List<Pointd> drawingList3 = new ArrayList<Pointd>();
        drawingList3.add(new Pointd(-1, 1, 1));
        drawingList3.add(new Pointd(1, 1, 1));
        drawingList3.add(new Pointd(1, 1, -1));
        drawingList3.add(new Pointd(-1, 1, -1));

        List<Pointd> drawingList4 = new ArrayList<Pointd>();
        drawingList4.add(new Pointd(-1, -1, 1));
        drawingList4.add(new Pointd(1, -1, 1));
        drawingList4.add(new Pointd(1, -1, -1));
        drawingList4.add(new Pointd(-1, -1, -1));

        Renderer renderer = new Renderer(canvas3d.getGraphicsContext2D(), -3d, 30d, canvas3d.getWidth(), canvas3d.getHeight());
        renderer.addDrawingGroup(drawingList1);
        renderer.addDrawingGroup(drawingList2);
        renderer.addDrawingGroup(drawingList3);
        renderer.addDrawingGroup(drawingList4);

        renderer.start();
        LOG.info("OK");
    }
}
