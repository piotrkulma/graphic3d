package pl.piotrkulma.graphic3d;

import javafx.scene.canvas.GraphicsContext;
import org.apache.log4j.Logger;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Piotr Kulma on 26.06.2017.
 */
public final class Renderer extends Thread {
    private final static Logger LOG = Logger.getLogger(Renderer.class);

    private double mx, my;

    private double d;
    private double scale;

    int[][] array2D;

    private GraphicsContext gc;

    private List<List<Pointd>> drawingList;

    public Renderer(GraphicsContext graphicsContext, double d, double scale, double width, double height) throws IOException {
        this.gc = graphicsContext;
        this.d = d;
        this.scale = scale;

        this.mx = width / 2d;
        this.my = height / 2d;

        this.drawingList = new ArrayList<>();
    }

    public void addDrawingGroup(List<Pointd> list) {
        drawingList.add(list);
    }

    @Override
    public void run() {
        super.run();

        while(true) {
            drawDrawingGroups();

            try {
                Thread.sleep(40);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void drawDrawingGroups() {
        gc.clearRect(0, 0, 500, 500);

        Iterator<List<Pointd>> iterator = drawingList.iterator();

        while(iterator.hasNext()) {
            List<Pointd> p3dList = iterator.next();
            double px[] = new double[4];
            double py[] = new double[4];

            int i=0;
            for(Pointd p3d : p3dList) {
                double p2d[] = FloatMathUtils.projection(p3d, d, scale, mx, my);
                drawPoint(gc, p2d);

                px[i] = p2d[0];
                py[i] = p2d[1];
                i++;

                p3d.copy(FloatMathUtils.rotateX(p3d, 3d));
            }
            gc.strokeLine(px[0], py[0], px[1], py[1]);
            gc.strokeLine(px[1], py[1], px[2], py[2]);
            gc.strokeLine(px[2], py[2], px[0], py[0]);

            gc.strokeLine(px[0], py[0], px[3], py[3]);
            gc.strokeLine(px[3], py[3], px[2], py[2]);
        }
    }

    private void drawPoint(GraphicsContext gc, double... p) {
        gc.strokeLine(p[0], p[1], p[0]+1, p[1]+1);
    }
}
