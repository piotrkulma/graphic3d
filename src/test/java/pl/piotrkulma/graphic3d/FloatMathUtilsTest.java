package pl.piotrkulma.graphic3d;

import org.apache.log4j.Logger;
import org.junit.Test;

/**
 * Created by Piotr Kulma on 26.06.2017.
 */
public class FloatMathUtilsTest {
    private final static Logger LOG = Logger.getLogger(FloatMathUtilsTest.class);

    @Test
    public void multipleMatrix() {
        float[][] matrixA =
                {
                        {1, 0, 2},
                        {-1, 3, 1},
                        {-1, 3, 1},
                        {-1, 3, 1}
                };

        float[][] matrixB =
                {
                        {3},
                        {2},
                        {2}
                };

        for(int i=0; i<3; i++) {
            LOG.info(matrixA[0][i]);
        }
    }
}
